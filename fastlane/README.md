fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios staging
```
fastlane ios staging
```
Build Staging App for Testing
### ios staging_fast
```
fastlane ios staging_fast
```
Build Staging App for Testing without compressing.
### ios staging_unsigned
```
fastlane ios staging_unsigned
```
Build Unsigned Staging App for Testing
### ios staging_fast_unsigned
```
fastlane ios staging_fast_unsigned
```
Build Unsigned Staging App for Testing without compressing.
### ios staging_app_store
```
fastlane ios staging_app_store
```
Build Staging App for Distribution on the App Store
### ios staging_release_fast
```
fastlane ios staging_release_fast
```
Build Staging App for Distribution on the App Store without compressing
### ios staging_adhoc
```
fastlane ios staging_adhoc
```
Build Staging App for Ad Hoc Distribution
### ios staging_adhoc_fast
```
fastlane ios staging_adhoc_fast
```
Build Staging App for Ad Hoc Distribution without compressing
### ios staging_app_store_upload
```
fastlane ios staging_app_store_upload
```
Upload Staging App to the App Store
### ios qa
```
fastlane ios qa
```
Build QA App for Testing
### ios qa_fast
```
fastlane ios qa_fast
```
Build QA App for Testing without compressing.
### ios qa_unsigned
```
fastlane ios qa_unsigned
```
Build Unsigned QA App for Testing
### ios qa_fast_unsigned
```
fastlane ios qa_fast_unsigned
```
Build Unsigned QA App for Testing without compressing.
### ios qa_app_store
```
fastlane ios qa_app_store
```
Build QA App for Distribution on the App Store
### ios qa_release_fast
```
fastlane ios qa_release_fast
```
Build QA App for Distribution on the App Store without compressing
### ios qa_adhoc
```
fastlane ios qa_adhoc
```
Build QA App for Ad Hoc Distribution
### ios qa_adhoc_fast
```
fastlane ios qa_adhoc_fast
```
Build QA App for Ad Hoc Distribution without compressing
### ios qa_app_store_upload
```
fastlane ios qa_app_store_upload
```
Upload QA App to the App Store
### ios demo
```
fastlane ios demo
```
Build Demo App for Testing
### ios demo_fast
```
fastlane ios demo_fast
```
Build Demo App for Testing without compressing.
### ios demo_unsigned
```
fastlane ios demo_unsigned
```
Build Unsigned Demo App for Testing
### ios demo_fast_unsigned
```
fastlane ios demo_fast_unsigned
```
Build Unsigned Demo App for Testing without compressing.
### ios demo_app_store
```
fastlane ios demo_app_store
```
Build Demo App for Distribution on the App Store
### ios demo_release_fast
```
fastlane ios demo_release_fast
```
Build Demo App for Distribution on the App Store without compressing
### ios demo_adhoc
```
fastlane ios demo_adhoc
```
Build Demo App for Ad Hoc Distribution
### ios demo_adhoc_fast
```
fastlane ios demo_adhoc_fast
```
Build Demo App for Ad Hoc Distribution without compressing
### ios demo_app_store_upload
```
fastlane ios demo_app_store_upload
```
Upload Demo App to the App Store
### ios production
```
fastlane ios production
```
Build Production App for Testing
### ios production_fast
```
fastlane ios production_fast
```
Build Production App for Testing without compressing.
### ios production_unsigned
```
fastlane ios production_unsigned
```
Build Unsigned Production App for Testing
### ios production_fast_unsigned
```
fastlane ios production_fast_unsigned
```
Build Unsigned Production App for Testing without compressing.
### ios production_app_store
```
fastlane ios production_app_store
```
Build Production App for Distribution on the App Store
### ios production_release_fast
```
fastlane ios production_release_fast
```
Build Production App for Distribution on the App Store without compressing
### ios production_adhoc
```
fastlane ios production_adhoc
```
Build Production App for Ad Hoc Distribution
### ios production_adhoc_fast
```
fastlane ios production_adhoc_fast
```
Build Production App for Ad Hoc Distribution without compressing
### ios production_app_store_upload
```
fastlane ios production_app_store_upload
```
Upload Production App to the App Store
### ios release
```
fastlane ios release
```
Build Release App for Testing
### ios release_fast
```
fastlane ios release_fast
```
Build Release App for Testing without compressing.
### ios release_unsigned
```
fastlane ios release_unsigned
```
Build Unsigned Release App for Testing
### ios release_fast_unsigned
```
fastlane ios release_fast_unsigned
```
Build Unsigned Release App for Testing without compressing.
### ios release_app_store
```
fastlane ios release_app_store
```
Build Release App for Distribution on the App Store
### ios release_release_fast
```
fastlane ios release_release_fast
```
Build Release App for Distribution on the App Store without compressing
### ios release_adhoc
```
fastlane ios release_adhoc
```
Build Release App for Ad Hoc Distribution
### ios release_adhoc_fast
```
fastlane ios release_adhoc_fast
```
Build Release App for Ad Hoc Distribution without compressing
### ios release_app_store_upload
```
fastlane ios release_app_store_upload
```
Upload Release App to the App Store

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
