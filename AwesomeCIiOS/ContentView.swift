//
//  ContentView.swift
//  AwesomeCIiOS
//
//  Created by Robert Peralta on 6/9/20.
//  Copyright © 2020 Robert Peralta. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
